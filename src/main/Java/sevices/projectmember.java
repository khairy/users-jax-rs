package sevices;


import com.sun.javafx.beans.IDProperty;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "project", schema = "authdb")
public class projectmember implements Serializable {

    @Column(name = "PROJID")
    @Id
    private String projid;

    @ManyToMany
    @JoinTable(
            name = "employee",
            joinColumns = @JoinColumn(name = "EMPID"),
            inverseJoinColumns = @JoinColumn(name = "PROJID"))
    private employee employee;

    public String getProjid() {
        return projid;
    }

    public void setProjid(String projid) {
        this.projid = projid;
    }
}
