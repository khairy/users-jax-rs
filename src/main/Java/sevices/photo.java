package sevices;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "photo", schema = "authdb")
public class photo implements Serializable {

    @Column(name = "PHOTOID")
    @Id
    private int photoid;


    @OneToOne
    @JoinColumn(name = "EMPID")
    private employee employee;

    @Column(name = "IMAGENAME")
    private String imagename;

    public int getPhotoid() {
        return photoid;
    }

    public void setPhotoid(int photoid) {
        this.photoid = photoid;
    }
}
