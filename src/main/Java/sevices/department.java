package sevices;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "department", schema = "authdb")
public class department implements Serializable {

    @Column(name="DEPTCODE")
    @Id
    private String deptcode;

    @Column(name="DEPTNAME")
    private String deptname;

    @ManyToOne
   @JoinColumn(name = "EMPID")
    private employee employee;

    public department() {
    }

    public String getDeptcode() {
        return deptcode;
    }

    public void setDeptcode(String deptcode) {
        this.deptcode = deptcode;
    }
}
