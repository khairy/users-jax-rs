package sevices;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "phonenumber", schema = "authdb")
public class phonenumber implements Serializable {

    @Column(name = "PHONEID")
    @Id
    private int phoneid;

    @ManyToOne
    @JoinColumn(name = "EMPID")
    private employee employee;

    @Column(name = "LOCALNUM")
    private int localnum;

    @Column(name = "INTLPREFIX")
    private String intlprefix;

    @Column(name = "PHONETYPE")
    private String phonetype;

    public int getPhoneid() {
        return phoneid;
    }

    public void setPhoneid(int phoneid) {
        this.phoneid = phoneid;
    }
}
