package sevices;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "project", schema = "authdb")
public class project implements Serializable {

    @Column(name = "PROJID")
    @Id
    private String projid;

    @Column(name = "PROJNAME")
    private String projname;

    @Column(name = "STARTDATE")
    private Date startdate;

    @Column(name = "TARGETNAME")
    private Date targetdate;

    @Column(name = "STATUS")
    private String status;

    @Column(name = "DESCRIPTION")
    private String description;

    public String getProjid() {
        return projid;
    }

    public void setProjid(String projid) {
        this.projid = projid;
    }
}
