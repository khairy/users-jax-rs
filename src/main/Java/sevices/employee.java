package sevices;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Entity
@Table(name = "employee", schema = "authdb")
@XmlRootElement
public class employee implements Serializable {

    @Column(name="EMPID")
    @Id
    private String empid;

    @Column(name="DEPTCODE")
    private String deptcode;

    @Column(name="JOBTITLE")
    private String jobtitle;

    @Column(name="GIVENNAME")
    private String givenname;

    @Column(name="FAMILYNAME")
    private String familyname;

    @Column(name="COMMONNAME")
    private String commonname;

    @Column(name="NAMETITLE")
    private String nametitle;

    public employee() {
    }

    public String getEmpid() {
        return empid;
    }

    public void setEmpid(String empid) {
        this.empid = empid;
    }
}
