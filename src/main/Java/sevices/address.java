package sevices;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Table(name = "address", schema = "authdb")
public class address implements Serializable{

    @Column(name = "ADDRESSID")
    @Id
    private int addressid;

    @Column(name = "ADDLINE1")
    private String addline1 ;

    @Column(name = "ADDLINE2")
    private String addline2 ;

    @Column(name = "CITY")
    private String city ;

    @Column(name = "REGION")
    private String region ;

    @Column(name = "COUNTRY")
    private String country ;

    @Column(name = "POSTCODE")
    private String postcode ;

    @ManyToOne
    @JoinColumn(name = "EMPID")
    @NotNull
    private employee employee;

    public address() {
    }

    public int getAddressid() {
        return addressid;
    }

    public void setAddressid(int addressid) {
        this.addressid = addressid;
    }
}
