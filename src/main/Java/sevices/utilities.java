package sevices;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collection;
import java.util.List;

@Stateless
public class utilities {

    @PersistenceContext
    private EntityManager em;

    public utilities() {
    }

    public List<employee> getAllEmployees() {
        return em.createQuery("SELECT s FROM employee s", employee.class).
                getResultList();
    }

    public employee getEmployee(String empid) {
        return em.createQuery("SELECT s FROM employee s" +
                "  WHERE s.empid = :empid", employee.class).
                setParameter("empid", empid).
                getSingleResult();
    }
    public List<address> getAllAddress() {
        return em.createQuery("SELECT s FROM address s", address.class).
                getResultList();
    }

    public address getAddress(int addressid) {
        return em.createQuery("SELECT s FROM address s" +
                "  WHERE s.addressid = :addressid", address.class).
                setParameter("addressid", addressid).
                getSingleResult();
    }
    public List<department> getAllDepartment() {
        return em.createQuery("SELECT s FROM department s", department.class).
                getResultList();
    }

    public department getdepartment(String depcode) {
        return em.createQuery("SELECT s FROM department s" +
                "  WHERE s.depcode = :depcode", department.class).
                setParameter("depcode", depcode).
                getSingleResult();
    }
    public List<email> getAllEmail() {
        return em.createQuery("SELECT s FROM email s", email.class).
                getResultList();
    }

    public email getEmail(int emailid) {
        return em.createQuery("SELECT s FROM email s" +
                "  WHERE s.emailid = :emailid", email.class).
                setParameter("emailid", emailid).
                getSingleResult();
    }
    public List<phonenumber> getAllPhonenumber() {
        return em.createQuery("SELECT s FROM phonenumber s", phonenumber.class).
                getResultList();
    }

    public phonenumber getPhonenumber(int phoneid) {
        return em.createQuery("SELECT s FROM phoneid s" +
                "  WHERE s.phoneid = :phoneid", phonenumber.class).
                setParameter("phoneid", phoneid).
                getSingleResult();
    }
    public Collection<photo> getAllPhoto() {
        return em.createQuery("SELECT s FROM photo s", photo.class).
                getResultList();
    }

    public photo getPhoto(int photoid) {
        return em.createQuery("SELECT s FROM photo s" +
                "  WHERE s.empid = :empid", photo.class).
                setParameter("photoid", photoid).
                getSingleResult();
    }
    public Collection<project> getAllProject() {
        return em.createQuery("SELECT s FROM project s", project.class).
                getResultList();
    }

    public project getProject(String projid) {
        return em.createQuery("SELECT s FROM project s" +
                "  WHERE s.projid = :projid", project.class).
                setParameter("projid", projid).
                getSingleResult();
    }
    public List<projectmember> getAllProjectmember() {
        return em.createQuery("SELECT s FROM projectmember s", projectmember.class).
                getResultList();
    }

    public projectmember getProjectmember(String projid) {
        return em.createQuery("SELECT s FROM projectmember s" +
                "  WHERE s.projid = :projid", projectmember.class).
                setParameter("projid", projid).
                getSingleResult();
    }
    public address saveAddress(address entity) {
        return em.merge(entity);
    }

    public void deleteAddress(int addressid) {
        address address = em.find(address.class, addressid);
        if (address == null)
            throw new IllegalArgumentException("address with ID: " + addressid + "does not exist in the database");

        em.remove(address);
    }
    public department saveDepartment(department entity) {
        return em.merge(entity);
    }

    public void deleteDepartment(String deptcode) {
        department department = em.find(department.class, deptcode);
        if (department == null)
            throw new IllegalArgumentException("department with ID: " + deptcode + "does not exist in the database");

        em.remove(department);
    }
    public email saveEmail(email entity) {
        return em.merge(entity);
    }

    public void deleteEmail(int emailid) {
        email email = em.find(email.class, emailid);
        if (email == null)
            throw new IllegalArgumentException("email with ID: " + emailid + "does not exist in the database");

        em.remove(email);
    }
    public employee saveEmployee(employee entity) {
        return em.merge(entity);
    }

    public void deleteEmployee(String empid) {
        employee employee = em.find(employee.class, empid);
        if (employee == null)
            throw new IllegalArgumentException("employee with ID: " + empid + "does not exist in the database");

        em.remove(employee);
    }

    public phonenumber savePhonenumber(phonenumber entity) {
        return em.merge(entity);
    }

    public void deletePhonenumber(int phoneid) {
        phonenumber phonenumber = em.find(phonenumber.class, phoneid);
        if (phonenumber == null)
            throw new IllegalArgumentException("phonenumber with ID: " + phoneid + "does not exist in the database");

        em.remove(phonenumber);
    }

    public photo savePhoto(photo entity) {
        return em.merge(entity);
    }

    public void deletePhoto(int photoid) {
        photo photo = em.find(photo.class, photoid);
        if (photo == null)
            throw new IllegalArgumentException("photo with ID: " + photoid + "does not exist in the database");

        em.remove(photo);
    }

    public project saveProject(project entity) {
        return em.merge(entity);
    }

    public void deleteProject(String projid) {
        project project = em.find(project.class, projid);
        if (project == null)
            throw new IllegalArgumentException("project with ID: " + projid + "does not exist in the database");

        em.remove(project);
    }

    public projectmember saveProjectMember(projectmember entity) {
        return em.merge(entity);
    }

    public void deleteProjectMember(String projid) {
        projectmember projectmember = em.find(projectmember.class, projid);
        if (projectmember == null)
            throw new IllegalArgumentException("projectmember with ID: " + projid + "does not exist in the database");

        em.remove(projectmember);
    }


}

