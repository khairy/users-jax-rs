package sevices;

public class User {

    private String name;
    private long id;
    private String address;
    private String mail;

    public User(){
    }

    public User(String name, long id, String address, String mail) {
        this.name = name;
        this.id = id;
        this.address = address;
        this.mail = mail;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", id=" + id +
                ", address='" + address + '\'' +
                "mail=" + mail +
        '}' ;
    }
}
