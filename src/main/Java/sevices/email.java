package sevices;


import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "email", schema = "authdb")
public class email implements Serializable {

    @Id
    @Column(name = "EMAILID")
    private int emailid;

    @Column(name = "EMAILADDRESS")
    private String emailaddress;

    @Column(name = "EMAILTYPE")
    private String emailtype;

    @ManyToOne
    @JoinColumn(name = "EMPID")
    private employee employee;

    public email() {
    }

    public int getEmailid() {
        return emailid;
    }

    public void setEmailid(int emailid) {
        this.emailid = emailid;
    }
}
