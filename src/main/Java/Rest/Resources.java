package Rest;

import sevices.*;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import java.util.logging.Logger;

import static java.util.logging.Level.SEVERE;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;


@RequestScoped
@Produces(APPLICATION_JSON)
@Consumes(APPLICATION_JSON)
@Path("emp")

public class Resources {

    private static final Logger LOGGER = Logger.getLogger(utilities.class.getName());


    @EJB
    private utilities util;

    @Context
    HttpServletRequest request;

    @GET
    public Response getAllEmployess() {
        try {
            return Response.ok().
                    entity(util.getAllEmployees()).
                    build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().
                    entity(e.getClass() + ": " + e.getMessage()).
                    build();
        }
    }
    @GET
    @Path("address")
    public Response getAllAddresses() {
        try {
            return Response.ok().
                    entity(util.getAllAddress()).
                    build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().
                    entity(e.getClass() + ": " + e.getMessage()).
                    build();
        }
    }
    @GET
    @Path("department")
    public Response getAllDepartments() {
        try {
            return Response.ok().
                    entity(util.getAllDepartment()).
                    build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().
                    entity(e.getClass() + ": " + e.getMessage()).
                    build();
        }
    }
    @GET
    @Path("email")
    public Response getAllEmails() {
        try {
            return Response.ok().
                    entity(util.getAllEmail()).
                    build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().
                    entity(e.getClass() + ": " + e.getMessage()).
                    build();
        }
    }
    @GET
    @Path("phonenumber")
    public Response getAllPhonenumbers() {
        try {
            return Response.ok().
                    entity(util.getAllPhonenumber()).
                    build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().
                    entity(e.getClass() + ": " + e.getMessage()).
                    build();
        }
    }
    @GET
    @Path("photo")
    public Response getAllphotos() {
        try {
            return Response.ok().
                    entity(util.getAllPhoto()).
                    build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().
                    entity(e.getClass() + ": " + e.getMessage()).
                    build();
        }
    }
    @GET
    @Path("project")
    public Response getAllProject() {
        try {
            return Response.ok().
                    entity(util.getAllProject()).
                    build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().
                    entity(e.getClass() + ": " + e.getMessage()).
                    build();
        }
    }
    @GET
    @Path("projectmember")
    public Response getAllProjectMember() {
        try {
            return Response.ok().
                    entity(util.getAllProjectmember()).
                    build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().
                    entity(e.getClass() + ": " + e.getMessage()).
                    build();
        }
    }

    @GET
    @Path("address")
    public Response getAddressId(@QueryParam("addressid") int addressid) {
        try {
            return Response.ok().
                    entity(util.getAddress(addressid)).
                    build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().
                    entity(e.getClass() + ": " + e.getMessage()).
                    build();
        }
    }

    @GET
    @Path("department")
    public Response getDepatmentId(@QueryParam("deptid") String deptcode) {
        try {
            return Response.ok().
                    entity(util.getdepartment(deptcode)).
                    build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().
                    entity(e.getClass() + ": " + e.getMessage()).
                    build();
        }
    }

    @GET
    @Path("email")
    public Response getEmailId(@QueryParam("emailid") int emailid) {
        try {
            return Response.ok().
                    entity(util.getEmail(emailid)).
                    build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().
                    entity(e.getClass() + ": " + e.getMessage()).
                    build();
        }
    }

    @GET
    @Path("employee")
    public Response getEmployeeId(@QueryParam("empid") String empid) {
        try {
            return Response.ok().
                    entity(util.getEmployee(empid)).
                    build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().
                    entity(e.getClass() + ": " + e.getMessage()).
                    build();
        }
    }
    @GET
    @Path("phonenumber")
    public Response getPhonenumberId(@QueryParam("phoneid") int phoneid) {
        try {
            return Response.ok().
                    entity(util.getPhonenumber(phoneid)).
                    build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().
                    entity(e.getClass() + ": " + e.getMessage()).
                    build();
        }
    }

    @GET
    @Path("photo")
    public Response getPhoto(@QueryParam("photoid") int photoid) {
        try {
            return Response.ok().
                    entity(util.getPhoto(photoid)).
                    build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().
                    entity(e.getClass() + ": " + e.getMessage()).
                    build();
        }
    }
    @GET
    @Path("project")
    public Response getProject(@QueryParam("projid") String projid) {
        try {
            return Response.ok().
                    entity(util.getProject(projid)).
                    build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().
                    entity(e.getClass() + ": " + e.getMessage()).
                    build();
        }
    }
    @GET
    @Path("projmemb")
    public Response getProjectMember(@QueryParam("projid") String projid) {
        try {
            return Response.ok().
                    entity(util.getProjectmember(projid)).
                    build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().
                    entity(e.getClass() + ": " + e.getMessage()).
                    build();
        }
    }

    @POST
    @Path("address")
    public Response SaveAddress(address address) {
        try {
            return Response.ok().
                    entity(util.saveAddress(address)).
                    build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().
                    entity(e.getClass() + ": " + e.getMessage()).
                    build();
        }
    }

    @POST
    @Path("department")
    public Response saveDepatment(department department) {
        try {
            return Response.ok().
                    entity(util.saveDepartment(department)).
                    build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().
                    entity(e.getClass() + ": " + e.getMessage()).
                    build();
        }
    }

    @POST
    @Path("email")
    public Response saveEmail(email email) {
        try {
            return Response.ok().
                    entity(util.saveEmail(email)).
                    build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().
                    entity(e.getClass() + ": " + e.getMessage()).
                    build();
        }
    }

    @POST
    public Response saveEmployee(employee employee) {
        try {
            return Response.ok().
                    entity(util.saveEmployee(employee)).
                    build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().
                    entity(e.getClass() + ": " + e.getMessage()).
                    build();
        }
    }

    @POST
    @Path("phonenumber")
    public Response savePhonenumber(phonenumber phonenumber) {
        try {
            return Response.ok().
                    entity(util.savePhonenumber(phonenumber)).
                    build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().
                    entity(e.getClass() + ": " + e.getMessage()).
                    build();
        }
    }

    @POST
    @Path("photo")
    public Response savePhoto(photo photo) {
        try {
            return Response.ok().
                    entity(util.savePhoto(photo)).
                    build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().
                    entity(e.getClass() + ": " + e.getMessage()).
                    build();
        }
    }

    @POST
    @Path("project")
    public Response saveProject(project project) {
        try {
            return Response.ok().
                    entity(util.saveProject(project)).
                    build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().
                    entity(e.getClass() + ": " + e.getMessage()).
                    build();
        }
    }

    @POST
    @Path("projmemb")
    public Response saveProjectMember(projectmember projectmember) {
        try {
            return Response.ok().
                    entity(util.saveProjectMember(projectmember)).
                    build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().
                    entity(e.getClass() + ": " + e.getMessage()).
                    build();
        }
    }

    @DELETE
    @Path("address")
    public Response deleteAddressId(@QueryParam("addressid") int addressid) {
        try {
            util.deleteAddress(addressid);
            return Response.ok().
                    build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().
                    entity(e.getClass() + ": " + e.getMessage()).
                    build();
        }
    }


    @DELETE
    @Path("department")
    public Response deleteDepatmentId(@QueryParam("deptid") String deptcode) {
        try {
            util.deleteDepartment(deptcode);
            return Response.ok().
                    build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().
                    entity(e.getClass() + ": " + e.getMessage()).
                    build();
        }
    }

    @DELETE
    @Path("email")
    public Response deleteEmailId(@QueryParam("emailid") int emailid) {
        try {
            util.deleteEmail(emailid);
            return Response.ok().
                    build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().
                    entity(e.getClass() + ": " + e.getMessage()).
                    build();
        }
    }

    @DELETE
    public Response delteEmployeeId(@QueryParam("empid") String empid) {
        try {
            util.deleteEmployee(empid);
            return Response.ok().
                    build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().
                    entity(e.getClass() + ": " + e.getMessage()).
                    build();
        }
    }
    @DELETE
    @Path("phonenumber")
    public Response deletePhonenumberId(@QueryParam("phoneid") int phoneid) {
        try {
            util.deletePhonenumber(phoneid);
            return Response.ok().
                    build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().
                    entity(e.getClass() + ": " + e.getMessage()).
                    build();
        }
    }

    @DELETE
    @Path("photo")
    public Response deletePhoto(@QueryParam("photoid") int photoid) {
        try {
            util.deletePhoto(photoid);
            return Response.ok().
                    build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().
                    entity(e.getClass() + ": " + e.getMessage()).
                    build();
        }
    }
    @DELETE
    @Path("project")
    public Response deleteProject(@QueryParam("projid") String projid) {
        try {
            util.deleteProject(projid);
            return Response.ok().
                    build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().
                    entity(e.getClass() + ": " + e.getMessage()).
                    build();
        }
    }

    @DELETE
    @Path("projmemb")
    public Response deleteProjectMember(@QueryParam("projid") String projid) {
        try {
            util.deleteProjectMember(projid);
            return Response.ok().
                    build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().
                    entity(e.getClass() + ": " + e.getMessage()).
                    build();
        }

}
}