package Rest;

import sevices.User;
import sevices.UsersUtlities;

import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import java.util.logging.Logger;

import static java.util.logging.Level.SEVERE;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;


@RequestScoped
@Produces(APPLICATION_JSON)
@Consumes(APPLICATION_JSON)
@Path("user")

public class userresources {

    private static final Logger LOGGER = Logger.getLogger(UsersUtlities.class.getName());


    private static UsersUtlities util = new UsersUtlities();

    @Context
    HttpServletRequest request;

    @GET
    public Response getAllStudents() {
        try {
            return Response.ok().
                    entity(util.showUsers()).
                    build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().
                    entity(e.getClass() + ": " + e.getMessage()).
                    build();
        }
    }

    @GET
    @Path("id/{id}")
    public Response getStudentId(@PathParam("id") Long id) {
        try {
            return Response.ok().
                    entity(util.SearchUserId(id)).
                    build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().
                    entity(e.getClass() + ": " + e.getMessage()).
                    build();
        }
    }
    @GET
    @Path("name")
    public Response getStudentName(@QueryParam("name") String name) {
        try {
            return Response.ok().
                    entity(util.SearchUserName(name)).
                    build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().
                    entity(e.getClass() + ": " + e.getMessage()).
                    build();
        }
    }
    @GET
    @Path("address")
    public Response getStudentAddress(@QueryParam("address") String address) {
        try {
            return Response.ok().
                    entity(util.SearchUserAddress(address)).
                    build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().
                    entity(e.getClass() + ": " + e.getMessage()).
                    build();
        }
    }
    @GET
    @Path("mail")
    public Response getStudentMail(@QueryParam("mail") String mail) {
        try {
            return Response.ok().
                    entity(util.SearchUserMail(mail)).
                    build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().
                    entity(e.getClass() + ": " + e.getMessage()).
                    build();
        }
    }

    @DELETE
    @Path("deleteid")
    public Response Deleteuser (@QueryParam("id") long id) {
        try {
            return Response.ok().
                    entity(util.deleteUserID(id)).
                    build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().
                    entity(e.getClass() + ": " + e.getMessage()).
                    build();
        }
    }

    @POST
    @Path("adduser")
    public Response addUser (User user) {
        try {
            return Response.ok().
                    entity(util.addUser(user)).
                    build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().
                    entity(e.getClass() + ": " + e.getMessage()).
                    build();
        }
    }
    @POST
    @Path("edituser")
    public Response edit (User user) {
        try {
            return Response.ok().
                    entity(util.updateUser(user)).
                    build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().
                    entity(e.getClass() + ": " + e.getMessage()).
                    build();
        }
    }
}
